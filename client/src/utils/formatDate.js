export const calculateCreatedTime = (timeCreated) => {
  let periods = {
    "Years Ago": 365 * 30 * 24 * 60 * 60 * 1000,
    "Months Ago": 30 * 24 * 60 * 60 * 1000,
    "Weeks ago": 7 * 24 * 60 * 60 * 1000,
    "Days Ago": 24 * 60 * 60 * 1000,
    "Hours Ago": 60 * 60 * 1000,
    "Minute Ago": 60 * 1000,
  };

  let diff = Date.now() - +new Date(`${timeCreated}`);

  for (const key in periods) {
    if (diff >= periods[key]) {
      let result = Math.floor(diff / periods[key]);
      return `${result} ${result === 1 ? key : key}`;
    }
  }

  return "Now";
};
